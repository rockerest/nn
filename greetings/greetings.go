package greetings;

import(
	"fmt";
	"errors";
)

func Hello( name string ) ( string, error ) {
	var err error;
	var message string;

	err = nil;
	message = "";

	if( name == "" ){
		err = errors.New( "A name must be provided to be greeted." );
	} else {
		message = fmt.Sprintf( "Hi, %v. Welcome!", name );
	}

	return message, err;
}