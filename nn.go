package main

import (
	"fmt";
	"log";
	"errors";

	"example.com/greetings";
);

func setupLogger(){
	log.SetFlags( 0 );
}

func fatalFrom( modName string, line error ){
	log.Fatal( modName + ": " + line.Error() );
}

func logFrom( modName string, line string ){
	log.Print( modName + ": " + line );
}

func main(){
	setupLogger();

	greeting, err := greetings.Hello( "Thomas" );

	if( err != nil ){
		logFrom( "greetings", err.Error() );
		fatalFrom( "nn", errors.New( "Donezo..." ) );
	} else {
		fmt.Println( greeting );
	}
}

// NEXT UP https://go.dev/doc/tutorial/random-greeting